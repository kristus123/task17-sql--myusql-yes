package daos.deleteDAOs;

import daos.ContactEmailDAO;
import daos.ContactPhoneDAO;
import enums.TableEnum;
import models.ContactEmailModel;
import models.ContactModel;
import services.QueryHelperService;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static jdbc.DbConnector.conn;

public class ContactDeleteDAO {
    public static void deleteById(int id) throws SQLException {

        //deleting all emails
        ContactEmailDAO.getAllUserEmails(id).forEach(mail -> {
            try { ContactEmailDeleteDAO.delete(mail); }

            catch (SQLException e) { e.printStackTrace(); }
        });

        ContactPhoneDAO.findByContactForeinKey(id).forEach(number -> {
            try { ContactPhoneDeleteDAO.delete(number); }

            catch (SQLException e) { e.printStackTrace(); }
        });




    }

    public static void main(String... args) throws SQLException {
        deleteById(11);
    }
}
