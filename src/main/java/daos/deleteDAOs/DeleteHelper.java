package daos.deleteDAOs;

import enums.TableEnum;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import static jdbc.DbConnector.conn;

public class DeleteHelper {

    public static void deleteFromTableById(TableEnum table, int id) throws SQLException {
        Statement s = conn.createStatement();
        s.execute("SET FOREIGN_KEY_CHECKS=0");

        PreparedStatement ps = conn.prepareStatement("DELETE FROM " + table.getTableName() + " WHERE id = ?");

        ps.setInt(1, id);
        //System.out.println(ps.toString());
        //System.out.println(table.getTableName());
        ps.executeUpdate();

        s.execute("SET FOREIGN_KEY_CHECKS=1");
    }
}
