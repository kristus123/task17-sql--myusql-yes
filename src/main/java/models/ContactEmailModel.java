package models;

import daos.ContactDAO;
import enums.ContactEmailEnum;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ContactEmailModel {
    private int id;
    private String email;
    private ContactModel owner;

    public ContactEmailModel(String email) {
        this.email = email;
    }

    public ContactEmailModel(ResultSet rs) throws SQLException {
        this.id    = rs.getInt("id");
        this.email = rs.getString("email");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public ContactModel getOwner() {
        return owner;
    }

    public void setOwner(ContactModel owner) {
        this.owner = owner;
    }

    @Override
    public String toString() {
        return "ID: " +  id + " Email Address: " + email;
    }
}
