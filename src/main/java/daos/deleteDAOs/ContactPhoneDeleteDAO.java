package daos.deleteDAOs;

import enums.TableEnum;
import models.ContactPhoneModel;

import java.sql.SQLException;

public class ContactPhoneDeleteDAO {

    public static void delete(int id) throws SQLException {
        DeleteHelper.deleteFromTableById(TableEnum.CONTACT_PHONES, id);
    }

    public static void delete(ContactPhoneModel contactPhoneModel) throws SQLException {
        delete(contactPhoneModel.getId());
    }
}
