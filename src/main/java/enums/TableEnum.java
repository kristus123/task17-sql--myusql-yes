package enums;

import models.ContactModel;

public enum TableEnum {
    CONTACTS("contacts", ContactModel.class),
    CONTACT_EMAILS("contact_emails", ContactModel.class),
    CONTACT_PHONES("contact_phones", ContactModel.class),
    FAMILY_RELATIONSHIPS("family_relationships", ContactModel.class),
    HOME_ADDRESSES("home_addresses", ContactModel.class);

    private String tableName;
    private Class pojoToModel;

    TableEnum(String tableName, Class pojoToModel) {
        this.tableName   = tableName;
        this.pojoToModel = pojoToModel;
    }

    public String getTableName() {return tableName;}

    public Class getPojoToModel() {
        return pojoToModel;
    }


}
