package models;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface IModelInterfaces<T> {
    T generateNewObject(ResultSet rs) throws SQLException;
}
