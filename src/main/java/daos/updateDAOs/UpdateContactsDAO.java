package daos.updateDAOs;

import daos.ContactDAO;
import enums.ContactEnum;
import models.ContactModel;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static jdbc.DbConnector.conn;

public class UpdateContactsDAO {

    public static void main(String[] args) throws SQLException{


        ContactModel contact = ContactDAO.findById(15);

        String[][] values = new String[][] {{"first_name", "Tove"}, {"last_name", "Ultser"}};
        System.out.println("kommer hit");
        try{
            contact = updateContact(contact, values);
        } catch(NullPointerException ex) {
            System.out.println(ex.getMessage());
        }

        System.out.println(contact.getFirstName());
    }

    public static ContactModel updateContact(ContactModel model, String[][] newValue) throws SQLException, NullPointerException {
        StringBuilder sql = new StringBuilder("UPDATE contacts SET ");

        for(int i = 0; i < newValue.length; i++) {
            sql.append(newValue[i][0] + " = " + "'" + newValue[i][1] + "' ");
            if(!(i==newValue.length-1)) {
                sql.append(", ");
            }
        }

        sql.append(" WHERE id = ").append(model.getId());
        conn.prepareStatement(sql.toString().trim()).executeUpdate();

        return ContactDAO.findById(model.getId());
    }
}
