package daos;

import enums.ContactEnum;
import enums.TableEnum;
import models.ContactModel;
import services.QueryHelperService;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static jdbc.DbConnector.conn;

public class ContactDAO {
    public static List<ContactModel> findAll() throws SQLException {
        StringBuilder sqlQuery = new StringBuilder("SELECT * FROM " + TableEnum.CONTACTS.getTableName());
        List<ContactModel> liste = new ArrayList<>();

        PreparedStatement stmt= conn.prepareStatement(sqlQuery.toString());

        ResultSet rs = stmt.executeQuery();
        while (rs.next()) liste.add(new ContactModel(rs));

        return liste;
    }

    public static ContactModel findById(int id) throws SQLException, NullPointerException {
        //System.out.println(id);
        PreparedStatement ps = conn.prepareStatement("SELECT * FROM contacts WHERE id = ? ");
        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();

        //.out.println(ps.toString());

        if (rs.next() == false) return null;

        return new ContactModel(rs);
    }




    public static List<ContactModel> findByLastName(String surName) throws SQLException {
        return findAll().stream().filter(person -> person.getLastName().equalsIgnoreCase(surName)).collect(Collectors.toList());
    }

    public static ContactModel findByPhoneNumber(String phoneNumber) throws SQLException {
        return queryContactTable(new String[][]{ {"number" , phoneNumber}}).get(0);
    }


    public static List<ContactModel> findByFirstName(String firstName) throws SQLException {
        return queryContactTable(new String[][]{ {ContactEnum.FIRSTNAME.getColumnName() , firstName}});
    }




    private static List<ContactModel> queryContactTable(String[][] searchParams) throws SQLException {
        List<ContactModel> liste = new ArrayList<>();
        ResultSet rs = QueryHelperService.createSql(TableEnum.CONTACTS, searchParams);
        while (rs.next()) liste.add(new ContactModel(rs));
        return liste;
    }

}
