package enums;

public enum ContactEnum implements IEnumColumn {
    FIRSTNAME("first_name"),
    LASTNAME("last_name"),
    CONTACT_PHONES("contact_phones");

    ContactEnum(String columnName) {
        this.columnName = columnName;
    }

    private String columnName;
    public String getColumnName() {return columnName;}

    @Override
    public String getColumnId() {
        return "id";
    }
}
