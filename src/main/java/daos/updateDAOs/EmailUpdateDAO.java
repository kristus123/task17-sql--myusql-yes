package daos.updateDAOs;

import daos.ContactDAO;
import models.ContactEmailModel;
import models.ContactModel;
import models.ContactPhoneModel;

import java.sql.SQLException;

import static jdbc.DbConnector.conn;

public class EmailUpdateDAO {

    public static void main(String[] args) throws SQLException {
        ContactModel contact = ContactDAO.findById(16);

        updateEmail(contact.getEmails().get(0), "newEmail@tdizCool.com");

        updateEmailOwner(contact.getEmails().get(0), 19);


    }

    public static void updateEmail(ContactEmailModel email, String newValue) throws SQLException {
        String sql = "UPDATE contact_emails SET email = '" + newValue + "' WHERE contact_emails.id = " + email.getId();

        conn.prepareStatement(sql.trim()).executeUpdate();
        /*
        email.setEmail(newValue);
        System.out.println(email.getId());
        System.out.println(newValue);
         */
    }

    public static void updateEmailOwner(ContactEmailModel email, int newOwnerId) throws SQLException {
        //email.setId(ContactDAO.findById(newOwnerId));

        String sql = "UPDATE contact_emails SET contact_fk = " + newOwnerId + " WHERE contact_emails.id = " + email.getId();

        conn.prepareStatement(sql.trim()).executeUpdate();
    }
}
