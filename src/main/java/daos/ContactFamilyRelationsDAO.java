package daos;

import enums.TableEnum;
import models.ContactEmailModel;
import models.ContactModel;
import services.QueryHelperService;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ContactFamilyRelationsDAO {
    public static List<ContactModel> getAllFamilyRelations(int id) throws SQLException {

        return findUserRelations(new String[][] {{"contact_fk", String.valueOf(id)}} );

    }

    public static void main(String... args) throws SQLException {
        System.out.println("____________");
        System.out.println(
                getAllFamilyRelations(3)
        );
        System.out.println("____________");
    }


    private static List<ContactModel> findUserRelations(String[][] searchParams) throws SQLException {
        List<ContactModel> liste = new ArrayList<>();
        ResultSet rs = QueryHelperService.createSql(TableEnum.FAMILY_RELATIONSHIPS, searchParams);
        while (rs.next()) liste.add(ContactDAO.findById(rs.getInt("relation_fk")));


        return liste;
    }
}
