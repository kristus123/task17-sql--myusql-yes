package daos;

import enums.TableEnum;
import models.ContactEmailModel;
import models.ContactHomeAddress;
import models.ContactModel;
import services.QueryHelperService;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static jdbc.DbConnector.conn;

public class ContactEmailDAO {

    public static List<ContactEmailModel> getAllUserEmails(int id) throws SQLException {
        return queryTable(new String[][] { {"contact_fk", String.valueOf(id) } } );
    }

    public static List<ContactEmailModel> findAll() throws SQLException {
        StringBuilder sqlQuery = new StringBuilder("SELECT * FROM " + TableEnum.CONTACT_EMAILS.getTableName());
        List<ContactEmailModel> liste = new ArrayList<>();

        PreparedStatement stmt= conn.prepareStatement(sqlQuery.toString());

        ResultSet rs = stmt.executeQuery();
        while (rs.next()) liste.add(new ContactEmailModel(rs));

        return liste;
    }

    public static List<ContactEmailModel> getAllUserEmails(ContactModel contactModel) throws SQLException {
        return getAllUserEmails(contactModel.getId());
    }




    private static List<ContactEmailModel> queryTable(String[][] searchParams) throws SQLException {
        List<ContactEmailModel> liste = new ArrayList<>();
        ResultSet rs = QueryHelperService.createSql(TableEnum.CONTACT_EMAILS, searchParams);

        while (rs.next()) liste.add(new ContactEmailModel(rs));
        //System.out.println("asdkak");
        return liste;
    }

    private static List<ContactEmailModel> queryTable(int id) throws SQLException {
        List<ContactEmailModel> liste = new ArrayList<>();
        ResultSet rs = QueryHelperService.createSql(TableEnum.CONTACT_EMAILS, id);
        while (rs.next()) liste.add(new ContactEmailModel(rs));
        return liste;
    }
}
