package models;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ContactHomeAddress {
    private long id;
    private String address;

    public ContactHomeAddress(ResultSet rs) throws SQLException {
        this.id = rs.getLong("id");
        this.address = rs.getString("address");
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return id + " " + address;
    }
}
