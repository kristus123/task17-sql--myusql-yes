package daos.deleteDAOs;

import enums.TableEnum;

import java.sql.SQLException;

public class ContactHomeAddressDeleteDAO {
    public static void deleteById(int id) throws SQLException {
        DeleteHelper.deleteFromTableById(TableEnum.HOME_ADDRESSES, id);
    }
}
