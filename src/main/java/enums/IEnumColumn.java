package enums;

public interface IEnumColumn {
    String getColumnId();
    String getColumnName();
}
