package services;
import enums.IEnumColumn;
import enums.TableEnum;

import javax.swing.plaf.PanelUI;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import static jdbc.DbConnector.conn;


public class QueryHelperService {

    public static <T extends IEnumColumn> ResultSet createSql(TableEnum tableEnum , String[][] values) throws SQLException {
        StringBuilder sqlQuery = new StringBuilder("SELECT * FROM " + tableEnum.getTableName() + " WHERE ");

        for (String[] allParams : values) {
            sqlQuery.append(allParams[0] + " = " + "'" + allParams[1] + "' ");
        }

        //System.out.println(sqlQuery.toString().trim());
        return conn.prepareStatement(sqlQuery.toString().trim()).executeQuery();
    }


    public static ResultSet createSql(TableEnum tableEnum, int id) throws SQLException {
        String sql = "SELECT * FROM " + tableEnum.getTableName() + " WHERE id = ? ";
        PreparedStatement ps = conn.prepareStatement(sql.toString());
        ps.setInt(1, id);

        return ps.executeQuery();
    }

    public static int checkExistId(String sql, String ...value) throws SQLException{
        PreparedStatement stmt = conn.prepareStatement(sql);
        for(int i = 0; i<value.length; i++) {
            stmt.setString(i+1, value[i]);
        }
        ResultSet rs = stmt.executeQuery();

        if(rs.next()) {
            return rs.getInt(1);
        }
        return 0;
    }

}
