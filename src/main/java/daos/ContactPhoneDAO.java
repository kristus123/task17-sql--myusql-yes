package daos;

import enums.ContactEnum;
import enums.TableEnum;
import models.ContactModel;
import models.ContactPhoneModel;
import services.QueryHelperService;

import javax.xml.transform.Result;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static jdbc.DbConnector.conn;

public class ContactPhoneDAO {

    public static ContactPhoneModel findPhoneNumberByNumber(String phoneNumber) throws SQLException {
        StringBuilder sqlQuery = new StringBuilder("SELECT * FROM contact_phones WHERE number = ?");

        PreparedStatement ps = conn.prepareStatement(sqlQuery.toString().trim());
        ps.setString(1, phoneNumber);
        ResultSet rs = ps.executeQuery();

        if (rs.next() == false) return null;

        return new ContactPhoneModel(rs);
    }

    public static List<ContactPhoneModel> findAll() throws SQLException {
        StringBuilder sqlQuery = new StringBuilder("SELECT * FROM " + TableEnum.CONTACT_PHONES.getTableName());
        List<ContactPhoneModel> liste = new ArrayList<>();

        PreparedStatement stmt= conn.prepareStatement(sqlQuery.toString());

        ResultSet rs = stmt.executeQuery();
        while (rs.next()) liste.add(new ContactPhoneModel(rs));

        return liste;
    }



    public static List<ContactPhoneModel> findByContactForeinKey(int id) throws SQLException {
        return findById(id);
    }




    private static List<ContactPhoneModel> queryContactTable(String[][] searchParams) throws SQLException {
        List<ContactPhoneModel> liste = new ArrayList<>();
        ResultSet rs = QueryHelperService.createSql(TableEnum.CONTACT_PHONES, searchParams);
        while (rs.next()) liste.add(new ContactPhoneModel(rs));
        return liste;
    }

    public static List<ContactPhoneModel> findById(int id) throws SQLException, NullPointerException {
        List<ContactPhoneModel> liste = new ArrayList<>();
        PreparedStatement ps = conn.prepareStatement("SELECT * FROM contact_phones WHERE contact_fk = ? ");
        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();

        while (rs.next()) liste.add(new ContactPhoneModel(rs));

        return liste;
    }


}
