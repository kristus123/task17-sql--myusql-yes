package daos;

import services.QueryHelperService;

import java.sql.*;
import java.sql.Date;
import java.time.LocalDate;
import java.util.*;

import static jdbc.DbConnector.conn;
import static services.QueryHelperService.checkExistId;


public class InsertDAO {

    public static void insertAll(List<String> valuesList) throws SQLException {

        Boolean autoCommit = conn.getAutoCommit();
        int addressId;
        int contactId;

        try {
            conn.setAutoCommit(false);
            addressId = insertAddress(valuesList.get(2));
            contactId = insertContact(valuesList.get(0), valuesList.get(1), addressId, LocalDate.parse(valuesList.get(3)));
            insertPhoneNumber(contactId, Arrays.asList(valuesList.get(4)));
            insertEmails(contactId, Arrays.asList(valuesList.get(5)));
            //insertRelations(relationList);

            conn.commit();
        } catch(SQLException e) {
            System.out.println(e.toString());
            System.out.println("Rolled back");
            conn.rollback();
        } finally {
            conn.setAutoCommit(autoCommit);
        }
    }

    public static void insertPhoneNumber(int contactId, List<String> phoneNumbers) throws SQLException {
        for(String number: phoneNumbers) {
            putPhone(contactId, number);
        }
    }

    public static void insertEmails(int contactId, List<String> emailAddresses) throws SQLException {
        for(String email: emailAddresses) {
            putEmail(contactId, email);
        }
    }

    public static void insertRelations(String[][] relationList) throws SQLException {
        for(String[] relation: relationList) {
            int userId = Integer.parseInt(relation[1]);
            int relationId = Integer.parseInt(relation[2]);
            putRelations(userId, relationId, relation[0]);
        }
    }

    //returns id of address that was inserted
    public static int insertAddress(String address) throws SQLException {
        String checkSql = "SELECT id FROM home_addresses WHERE address=?";
        int id;
        if((id = checkExistId(checkSql, address)) > 0) {

          System.out.println("Address already exist in table with id " + id);
        } else {
        String sql = "INSERT INTO home_addresses (address) VALUES (?)";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, address);
        stmt.executeUpdate();
        }

        return checkExistId(checkSql, address);
    }

    public static int insertContact(String firstName, String lastName, String address, LocalDate dateOfBirth ) throws SQLException {
        int addressId = checkExistId("SELECT id FROM home_addresses WHERE address=?", address);
        return insertContact(firstName, lastName, addressId, dateOfBirth);
    }

    private static int insertContact(String firstName, String lastName, int addressId, LocalDate dateOfBirth) throws SQLException{
        Date date = Date.valueOf(dateOfBirth);

        String checkSql = "SELECT id FROM contacts WHERE first_name=? AND last_name=? AND home_address_fk=? AND date_of_birth=?";
        int contactId;
        if(((contactId = checkExistId(checkSql, firstName, lastName, Integer.toString(addressId), date.toString())) > 0)) {
            throw new SQLException("Contact already exists");
        }

        System.out.println("Id " + addressId);
        String sql = "INSERT INTO contacts (first_name, last_name, home_address_fk, date_of_birth) VALUE (?, ?, ?, ?)";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, firstName);
        stmt.setString(2, lastName);
        stmt.setInt(3, addressId);
        stmt.setDate(4, date);
        stmt.executeUpdate();
        return checkExistId(checkSql, firstName, lastName, Integer.toString(addressId), date.toString());
    }

    public static boolean putPhone(int contactID, String number) throws SQLException {
        String checkSQL = "SELECT id FROM contact_phones WHERE number=?";
        int id;
        if(((id = checkExistId(checkSQL, number)) > 0)) {
            throw new SQLException("Phone number already exists");
        }

        String sql = "INSERT INTO contact_phones (number, contact_fk) VALUE(?, ?)";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, number);
        stmt.setInt(2, contactID);
        stmt.executeUpdate();

        return true;
    }

    private static boolean putEmail(int contactID, String email) throws SQLException {
        String checkSQL = "SELECT id FROM contact_emails WHERE email=?";
        int id;
        if(((id = checkExistId(checkSQL, email)) > 0)) {
            throw new SQLException("Email already exists");
        }

        String sql = "INSERT INTO contact_emails (email, contact_fk) VALUE(?, ?)";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, email);
        stmt.setInt(2, contactID);
        stmt.executeUpdate();

        return true;
    }

    public static int putRelations(int userId, int relationsId, String relation) throws SQLException {
        String checkSQL = "SELECT id FROM family_relationships WHERE title=? AND contact_fk=? AND relation_fk=?";
        if(checkExistId(checkSQL, relation, Integer.toString(userId), Integer.toString(relationsId)) > 0) {
            throw new SQLException("Relationship already exists");
        }

        String sql = "INSERT INTO family_relationships (title, contact_fk, relation_fk) VALUE(?, ?, ?)";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, relation);
        stmt.setInt(2, userId);
        stmt.setInt(3, relationsId);
        stmt.executeUpdate();

        return checkExistId(checkSQL, relation, Integer.toString(userId), Integer.toString(relationsId));
    }
}
