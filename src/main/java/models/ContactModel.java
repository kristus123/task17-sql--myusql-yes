package models;

import daos.*;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class ContactModel {

    private String firstName;
    private String lastName;


    private List<ContactEmailModel> emails;

    private ContactHomeAddress address;

    private List<ContactPhoneModel> phoneNumbers;
    private int id;
    private Date dateOfBirth;

    private List<ContactModel> familyRelations;

    public ContactModel(ResultSet rs) throws SQLException {
        this.id = rs.getInt("id");
        this.firstName   = rs.getString("first_name");
        this.lastName    = rs.getString("last_name");
        this.emails       = ContactEmailDAO.getAllUserEmails(rs.getInt("id"));
        this.address      = ContactHomeAddressDAO.findHomeAddressByForeignKeyId(rs.getInt("home_address_fk"));
        this.dateOfBirth = rs.getDate("date_of_birth");

        this.familyRelations = ContactFamilyRelationsDAO.getAllFamilyRelations(rs.getInt("id"));
        this.phoneNumbers = ContactPhoneDAO.findByContactForeinKey(id);

        /*
        System.out.println("_________");
        System.out.println(id);
        System.out.println("_________");
         */
    }

    public void setFamilyRelations() throws SQLException {
        this.familyRelations = ContactFamilyRelationsDAO.getAllFamilyRelations(this.id);
    }

    public List<ContactEmailModel> getEmails() {
        return emails;
    }

    public void setEmails(List<ContactEmailModel> emails) {
        this.emails = emails;
    }

    public ContactHomeAddress getAddress() {
        return address;
    }

    public void setAddress(ContactHomeAddress address) {
        this.address = address;
    }

    public List<ContactPhoneModel> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(List<ContactPhoneModel> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public List<ContactModel> getFamilyRelations() {
        return familyRelations;
    }

    public void setFamilyRelations(List<ContactModel> familyRelations) {
        this.familyRelations = familyRelations;
    }

    public String getFirstName() {
        return firstName;
    }

    @Override
    public String toString() {
        String text = "------\nID: " + id + "\n" + firstName + " " + lastName + "\n" + address.getAddress() + "\n" + dateOfBirth.toString();
        StringBuilder sb = new StringBuilder(text).append("\nEmail addresses:");
        for(ContactEmailModel email: emails) {
            sb.append(" ").append(email.getEmail()).append(",");
        }
        sb.append("\nPhone numbers:");
        for(ContactPhoneModel phone: phoneNumbers) {
            sb.append(" ").append(phone.getPhoneNumber()).append(",");
        }
        sb.append("\nFamily relations:\n");
        for(ContactModel relation: familyRelations) {
            sb.append(relation.getFirstName()).append(" ").append(relation.getLastName()).append("\n");
        }
        return sb.toString();
    }

    public String smallPrint() {
        return "ID: " + id + " NAME: " + firstName + " " + lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }
}
