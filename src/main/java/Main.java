
import daos.*;

import daos.deleteDAOs.ContactDeleteDAO;
import daos.updateDAOs.AddressUpdateDAO;
import daos.updateDAOs.EmailUpdateDAO;
import daos.updateDAOs.PhonesUpdateDAO;
import daos.updateDAOs.UpdateContactsDAO;
import models.ContactEmailModel;
import models.ContactHomeAddress;
import models.ContactPhoneModel;

import java.awt.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import static jdbc.DbConnector.conn;

class Main{

    static Scanner scanner = null;
    public static void main(String args[]) throws SQLException {

        scanner = new Scanner(System.in);
        String input  = "";
        while(true) {
            try {
                System.out.println("What do you want to do?\n1: Search\n2: Update\n3: Create\n4: Delete\nWrite 'quit' to exit program");
                input = scanner.nextLine();

             if(input.equalsIgnoreCase("quit")) {
                    break;
                }
                selectCRUD(input);
            } catch(SQLException ex) {
                System.out.println(ex.getMessage());
            } catch(NumberFormatException ex) {
                System.out.println("You must choose a valid number for ID");
            }
        }
        conn.close(); // DON'T REMOVE

    }

    private static void selectCRUD(String input) throws SQLException, NumberFormatException{
        switch(input) {
            case "1":
                searchContact();
                break;
            case "2":
                update();
                break;
            case "3":
                create();
                break;
            case "4":
                delete();
                break;
            default:
                System.out.println("You must choose either 1, 2, 3 or 4");
                break;
        }
    }

    private static void delete() throws SQLException, NumberFormatException {
        ContactDAO.findAll().forEach((contact) -> System.out.println(contact.smallPrint()));
        System.out.println("Write the ID for the contact you want to delete");
        int id = Integer.parseInt(scanner.nextLine());
        ContactDeleteDAO.deleteById(id);
    }

    private static void searchContact() throws SQLException, NumberFormatException {
        System.out.println("What do you want to search?\n1: Show all\n2: Search by first name\n3: Search by last name\n4: Search by phonenumber");
        String input = scanner.nextLine();


        switch(input) {
            case "1": // find all
                ContactDAO.findAll().forEach(System.out::println);
                break;
            case "2": //search by first name
                System.out.println("type in the first name");
                ContactDAO.findByFirstName(scanner.nextLine()).forEach(System.out::println);
                break;
            case "3": //find by last name
                System.out.println("type in the last name");
                input = scanner.nextLine();
                ContactDAO.findByLastName(input).forEach(System.out::println); //i did this method in a lazy way
                break;
            case "4": //find by phone number
                System.out.println("type in the phone number");

                ContactPhoneModel contact = ContactPhoneDAO.findPhoneNumberByNumber(scanner.nextLine());
                if (contact == null) System.out.println("couldn't find");
                else System.out.println(contact.getPhoneOwner());


                break;
            default:

                break;
        }
    }

    private static void update() throws SQLException, NumberFormatException{
        System.out.println("What do you want to update?\n1: Contact\n2: PhoneNumber\n3: Address\n4: Email\n 5: Relation");
        String input = scanner.nextLine();
        switch(input) {
            case "1": //Updates Contact info
                updateContact();
                break;
            case "2": //Updates phonesnumbers
                updatePhone();
                break;
            case "3": //Updates address
                updateAddress();
                break;
            case "4": //Updates emails
                updateEmails();
                break;
            case "5": //Updates relations

                break;
            default:
                System.out.println("You must choose either 1, 2, 3 or 4");
                break;
        }
    }

    private static void updateEmails() throws SQLException, NumberFormatException {
        List<ContactEmailModel> emailList = ContactEmailDAO.findAll();
        emailList.forEach(System.out::println);

        System.out.println("What email address do you want to update? Write the ID");
        int id = Integer.parseInt(scanner.nextLine());
        System.out.println("Write the new email address");
        String newValue = scanner.nextLine();
        for(ContactEmailModel email: emailList) {
            if(email.getId() == id) {
                EmailUpdateDAO.updateEmail(email, newValue);
                return;
            }
        }
        System.out.println("Could not find email address with given ID");
    }

    private static void updatePhone() throws SQLException, NumberFormatException {
        List<ContactPhoneModel> phoneList = ContactPhoneDAO.findAll();
        phoneList.forEach(System.out::println);

        System.out.println("What number do you want to update? Write the ID");
        int id = Integer.parseInt(scanner.nextLine());
        System.out.println("Write the new number");
        String newValue = scanner.nextLine();
        for(ContactPhoneModel number: phoneList) {
            if(number.getId() == id) {
                PhonesUpdateDAO.updatePhoneNumber(number, newValue);
                return;
            }
        }
        System.out.println("Could not find phonenumber with given ID");
    }

    private static void updateAddress() throws SQLException, NumberFormatException {
        List<ContactHomeAddress> addressList = ContactHomeAddressDAO.findAll();
        addressList.forEach(System.out::println);
        System.out.println("What address do you want to update? Write the ID");
        long id = Long.parseLong(scanner.nextLine());
        System.out.println("What do you want to change to?");
        String newValue = scanner.nextLine();
        for(ContactHomeAddress address: addressList) {
            if(address.getId() == id) {
                AddressUpdateDAO.updateAddress(address, newValue);
                return;
            }
        }
        System.out.println("Could not find address with given ID");


    }

    private static void updateContact() throws SQLException, NumberFormatException {
        ContactDAO.findAll().forEach(System.out::println);
        System.out.println("Choose id of the one you would like to update");
        int id = Integer.parseInt(scanner.nextLine());
        System.out.println("What do you want to update?\n1: firstname\n2: lastname\n3: address \n4: dateofbirth");
        String input = scanner.nextLine();
        switch(input) {
            case "1":
                updateHelper("first_name", id);
                break;
            case "2":
                updateHelper("last_name", id);
                break;
            case "3":
                updateAddress(id);
                break;
            case "4":
                System.out.println("Write the date in this format\nYEAR-MONTH-DAY");
                updateHelper("date_of_birth", id);
                break;
            default:
                System.out.println("Choose legal number next time");
                break;
        }

    }

    private static void updateAddress(int personId) throws SQLException, NumberFormatException {
        System.out.println("What do you want to update to?");
        String newValue = scanner.nextLine();

        int addressId = InsertDAO.insertAddress(newValue);
        String[][] valuePair = {{"home_address_fk", addressId+""}};
        System.out.println(UpdateContactsDAO.updateContact(ContactDAO.findById(personId), valuePair));
    }

    private static void updateHelper(String updateCol, int id) throws SQLException, NumberFormatException {
        System.out.println("What do you want to update to?");
        String newValue = scanner.nextLine();
        String[][] valuePair = {{updateCol, newValue}};
        System.out.println(UpdateContactsDAO.updateContact(ContactDAO.findById(id), valuePair));

    }

    private static void create() throws SQLException, NumberFormatException {
        System.out.println("1: Create contact\n2: Create relation\n3: Create Number");
        String input = scanner.nextLine();
        switch(input) {
            case "1":
                createContact();
                break;
            case "2" :
                createRelation();
                break;
            case "3":
                createNumber();
                break;
      }
    }

    public static void createContact() throws SQLException, NumberFormatException {
        List<String> labelList = Arrays.asList("first name", "fast name", "address", "fate of Birth", "phone number", "email address");
        List<String> values = createHelper(labelList);
        InsertDAO.insertAll(values);
    }

    private static void createRelation() throws SQLException, NumberFormatException {
        ContactDAO.findAll().forEach(System.out::println);
        List<String> labelList = Arrays.asList("relation from", "relation to", "relation description");
        List<String> values = createHelper(labelList);
        int relationFrom = 0;
        int relationTo = 0;
        try {
            relationFrom = Integer.parseInt(values.get(0));
            relationTo = Integer.parseInt(values.get(1));
        } catch (NumberFormatException ex) {
            System.out.println("Need to choose a number for ID");
        }
        InsertDAO.putRelations(relationFrom, relationTo, values.get(2));
     }

      private static void createNumber() throws SQLException, NumberFormatException{
        System.out.println("Who do you want to add a number to, write the ID");
        int id = Integer.parseInt(scanner.nextLine());
        System.out.println("What phone number should be added?");
        String newNumber = scanner.nextLine();
        InsertDAO.putPhone(id, newNumber);
      }

     private static List<String> createHelper(List<String> labelList) {
        List<String> values = new ArrayList<>();
         for(String column: labelList) {
             System.out.println("Write " + column);
             values.add(scanner.nextLine());
         }
         return values;
     }
}