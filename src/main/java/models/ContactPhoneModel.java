package models;

import daos.ContactDAO;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ContactPhoneModel {
    private int id;
    private String phoneNumber;
    private ContactModel phoneOwner;
    private int phoneOwnerId;

    public ContactPhoneModel(ResultSet rs) throws SQLException {
        this.id = rs.getInt("id");
        this.phoneNumber = rs.getString("number");
        this.phoneOwnerId  = rs.getInt("contact_fk");
    }



    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public ContactModel getPhoneOwner() throws SQLException {
        return ContactDAO.findById(this.phoneOwnerId);
    }

    public void setPhoneOwner(ContactModel phoneOwner) {
        this.phoneOwner = phoneOwner;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return id + " " + phoneNumber + " OwnerID: " + phoneOwnerId;
    }
}
