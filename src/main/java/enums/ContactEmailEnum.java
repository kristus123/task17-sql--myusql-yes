package enums;

public enum ContactEmailEnum implements IEnumColumn {
    EMAIL("email"),
    Id("id");


    private String emailColumn;

    public String getColumnId() {
        return "id";
    }

    ContactEmailEnum(String emailColumn) {
        this.emailColumn = emailColumn;
    }

    public String getEmailColumn() {
        return emailColumn;
    }

    @Override
    public String getColumnName() {
        return this.emailColumn;
    }

}
