package daos.updateDAOs;

import daos.ContactHomeAddressDAO;
import models.ContactEmailModel;
import models.ContactHomeAddress;

import java.sql.SQLException;

import static jdbc.DbConnector.conn;

public class AddressUpdateDAO {

    public static void main(String[] args) throws SQLException {
        ContactHomeAddress address = ContactHomeAddressDAO.findHomeAddressByForeignKeyId(7);

        updateAddress(address, "Baker Street");
    }

    public static void updateAddress(ContactHomeAddress address, String newValue) throws SQLException {
        String sql = "UPDATE home_addresses SET address = '" + newValue + "' WHERE id = " + address.getId();

        conn.prepareStatement(sql.trim()).executeUpdate();
    }
}
