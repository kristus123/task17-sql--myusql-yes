package daos.deleteDAOs;

import enums.TableEnum;
import models.ContactEmailModel;

import java.sql.SQLException;

public class ContactEmailDeleteDAO {
    public static void delete(int id) throws SQLException {
        DeleteHelper.deleteFromTableById(TableEnum.CONTACT_EMAILS, id);
    }

    public static void delete(ContactEmailModel email) throws SQLException {
        delete(email.getId());
    }
}
