package daos.updateDAOs;

import daos.ContactDAO;
import models.ContactModel;
import models.ContactPhoneModel;

import java.sql.SQLException;
import java.util.List;

import static jdbc.DbConnector.conn;

public class PhonesUpdateDAO {

    public static void main(String[] args) throws SQLException {
        List<ContactPhoneModel> phoneNumber = ContactDAO.findById(15).getPhoneNumbers();

        //updatePhoneNumber(phoneNumber.get(0), "1010101010");

        updatePhoneOwner(phoneNumber.get(0), 19);
    }

    public static void updatePhoneNumber(ContactPhoneModel phoneNumber, String newValue) throws SQLException {
        String sql = "UPDATE contact_phones SET number = " + newValue + " WHERE id = " + phoneNumber.getId();

        conn.prepareStatement(sql.trim()).executeUpdate();
        phoneNumber.setPhoneNumber(newValue);
    }

    public static void updatePhoneOwner(ContactPhoneModel phoneNumber, int newOwnerId) throws SQLException {

        String sql = "UPDATE contact_phones SET contact_fk = " + newOwnerId + " WHERE id = " + phoneNumber.getId();

        conn.prepareStatement(sql.trim()).executeUpdate();
        /*

        ContactModel contact = ContactDAO.findById(newOwnerId);

        //removes phone number from old owners list
        phoneNumber.getPhoneOwner().getPhoneNumbers().remove(phoneNumber);
        //adds phone number to new owners list
        phoneNumber.setPhoneOwner(contact);
        contact.getPhoneNumbers().add(phoneNumber);
         */
    }
 }
