package daos;

import enums.TableEnum;
import models.ContactEmailModel;
import models.ContactHomeAddress;
import services.QueryHelperService;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static jdbc.DbConnector.conn;

public class ContactHomeAddressDAO {

    public static ContactHomeAddress findHomeAddressByForeignKeyId(int id) throws SQLException {
        return queryTable(id);
    }

    public static List<ContactHomeAddress> findAll() throws SQLException {
        StringBuilder sqlQuery = new StringBuilder("SELECT * FROM " + TableEnum.HOME_ADDRESSES.getTableName());
        List<ContactHomeAddress> liste = new ArrayList<>();

        PreparedStatement stmt= conn.prepareStatement(sqlQuery.toString());

        ResultSet rs = stmt.executeQuery();
        while (rs.next()) liste.add(new ContactHomeAddress(rs));

        return liste;
    }

    private static ContactHomeAddress queryTable(int id) throws SQLException {

        ResultSet rs = QueryHelperService.createSql(TableEnum.HOME_ADDRESSES, id);

        if (!rs.next()) return null;

        return new ContactHomeAddress(rs);
    }
}
